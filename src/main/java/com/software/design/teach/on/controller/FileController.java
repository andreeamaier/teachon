package com.software.design.teach.on.controller;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping("/add/{courseId}")
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<?> uploadFile(@RequestParam MultipartFile file, @RequestParam String title, @PathVariable Long courseId) throws BusinessException {
        fileService.addNewFile(file, title, courseId);

        return ResponseEntity.ok().build();
    }

}
