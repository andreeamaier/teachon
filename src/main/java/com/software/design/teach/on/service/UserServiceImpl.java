package com.software.design.teach.on.service;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.model.Role;
import com.software.design.teach.on.model.User;
import com.software.design.teach.on.model.dto.StudentDto;
import com.software.design.teach.on.model.dto.UserLoginDTO;
import com.software.design.teach.on.model.dto.UserRegisterDTO;
import com.software.design.teach.on.repository.RoleRepository;
import com.software.design.teach.on.repository.UserRepository;
import com.software.design.teach.on.security.JwtTokenProvider;
import com.software.design.teach.on.util.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public StudentDto getStudentById(Long id) throws BusinessException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()){
            return ObjectMapper.userEntityToStudentDto(user.get());
        } else {
            throw new BusinessException(404, "User not found.");
        }
    }

    @Override
    @Transactional
    public void registerUser(UserRegisterDTO user) throws BusinessException {
        if (existsByEmail(user.getEmail())) {
            throw new BusinessException(400, "Email address already taken.");
        }
        if (Objects.isNull(user)) {
            throw new BusinessException(401, "Body cannot be null!");
        }
        if (Objects.isNull(user.getEmail()) || user.getEmail().equals("")) {
            throw new BusinessException(400, "Email cannot be null ! ");
        }
        if (Objects.isNull(user.getPassword()) || user.getPassword().equals("")) {
            throw new BusinessException(400, "Password cannot be null !");
        }
        if (Objects.isNull(user.getFirstName()) || user.getFirstName().equals("")) {
            throw new BusinessException(400, "First name cannot be null ! ");
        }
        if (Objects.isNull(user.getLastName()) || user.getLastName().equals("")) {
            throw new BusinessException(400, "Last name cannot be null !");
        }

        User dbUser = ObjectMapper.userDtoToEntityMapper(user);
        dbUser.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByRole(user.getRole())
                .orElseThrow(() -> new BusinessException(403, "User Role not set."));
        dbUser.setRoles(Collections.singleton(userRole));
        userRepository.save(dbUser);
    }

    @Override
    @Transactional
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    @Transactional
    public String login(UserLoginDTO userLoginDTO) throws BusinessException {
        if (Objects.isNull(userLoginDTO)) {
            throw new BusinessException(401, "Body cannot be null!");
        }
        if (Objects.isNull(userLoginDTO.getEmail()) || userLoginDTO.getEmail().equals("")) {
            throw new BusinessException(400, "Email cannot be null! ");
        }
        if (Objects.isNull(userLoginDTO.getPassword()) || userLoginDTO.getPassword().equals("")) {
            throw new BusinessException(400, "Password cannot be null!");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userLoginDTO.getEmail(),
                        userLoginDTO.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return tokenProvider.generateToken(authentication);
    }

    @Override
    public StudentDto changeProfile(StudentDto studentDto, Long id) throws BusinessException {
        List<User> users = userRepository.findAll();
        User user = userRepository.getById(id);
        List<String> emails = users.stream().map(User::getEmail).collect(Collectors.toList());
        if (emails.contains(studentDto.getEmail())){
            throw new BusinessException(400, "Email already taken");
        } else {
            user.setEmail(studentDto.getEmail());
            user.setFirstName(studentDto.getFirstName());
            user.setLastName(studentDto.getLastName());

            userRepository.saveAndFlush(user);
            return ObjectMapper.userEntityToStudentDto(user);
        }
    }
}
