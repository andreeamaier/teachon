package com.software.design.teach.on.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "DOMAIN")
public class Domain {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "domain")
    private Set<Course> courses;

    @ManyToMany(mappedBy = "domainsOfInterest")
    private Set<User> users;

    public Domain() {
    }

    public Domain(String name, Set<Course> courses, Set<User> users) {
        this.name = name;
        this.courses = courses;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
