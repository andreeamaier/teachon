package com.software.design.teach.on.service;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.model.dto.StudentDto;
import com.software.design.teach.on.model.dto.UserLoginDTO;
import com.software.design.teach.on.model.dto.UserRegisterDTO;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    StudentDto getStudentById(Long id) throws BusinessException;

    void registerUser(UserRegisterDTO user) throws BusinessException;

    boolean existsByEmail(String email);

    String login(UserLoginDTO userLoginDTO) throws BusinessException;

    StudentDto changeProfile(StudentDto studentDto, Long id) throws BusinessException;
}
