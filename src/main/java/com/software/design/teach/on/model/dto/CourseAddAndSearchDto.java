package com.software.design.teach.on.model.dto;

import com.software.design.teach.on.model.Domain;

public class CourseAddAndSearchDto {

    private String name;

    public CourseAddAndSearchDto() {
    }

    public CourseAddAndSearchDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
