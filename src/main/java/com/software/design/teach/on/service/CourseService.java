package com.software.design.teach.on.service;

import com.software.design.teach.on.model.User;
import com.software.design.teach.on.model.dto.CourseAddAndSearchDto;
import com.software.design.teach.on.model.dto.CourseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {

    void addCourse(CourseAddAndSearchDto courseDto, Long domainId);

    void enrollToCourse(Long courseId, User user);

    void unenrollFromCourse(Long courseId, User user);

    List<CourseAddAndSearchDto> findCourse(String coursePartialName);
}
