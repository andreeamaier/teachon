package com.software.design.teach.on.model.dto;

import java.util.Set;

public class CourseDto extends CourseAddAndSearchDto {

    private Set<StudentDto> students;
    private Set<TeacherDto> teachers;
    private Set<FileUploadDto> files;
    private DomainDto domainDto;

    public CourseDto() {
    }

    public Set<StudentDto> getStudents() {
        return students;
    }

    public void setStudents(Set<StudentDto> students) {
        this.students = students;
    }

    public Set<TeacherDto> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<TeacherDto> teachers) {
        this.teachers = teachers;
    }

    public Set<FileUploadDto> getFiles() {
        return files;
    }

    public void setFiles(Set<FileUploadDto> files) {
        this.files = files;
    }

    public DomainDto getDomainDto() {
        return domainDto;
    }

    public void setDomainDto(DomainDto domainDto) {
        this.domainDto = domainDto;
    }
}
