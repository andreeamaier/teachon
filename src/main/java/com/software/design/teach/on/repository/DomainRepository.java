package com.software.design.teach.on.repository;

import com.software.design.teach.on.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DomainRepository extends JpaRepository<Domain, Long> {
}
