package com.software.design.teach.on.service;

import com.software.design.teach.on.model.Course;
import com.software.design.teach.on.model.User;
import com.software.design.teach.on.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class EmailService {

    private static final String FROM = "teachon@noreply.com";
    private static final String SUBJECT_NEW_COURSE = "New course added";
    private static final String SUBJECT_NEW_STUDENT = "New student enrolled";

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    EmailConfig emailConfig;

    public String composeNewCourseEmailBody(User user, String courseTitle) throws IOException {
        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("name", user.getFirstName());
        templateModel.put("course", courseTitle);
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);
        String htmlBody = templateEngine.process("new-course-email-template.html", thymeleafContext);

        return htmlBody;
    }

    public String composeNewStudentEmailBody(Course course, User user) throws IOException {
        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("teacher", course.getTeacher().getFirstName());
        templateModel.put("student", user.getFirstName() + " " + user.getLastName());
        templateModel.put("course", course.getName());
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);
        String htmlBody = templateEngine.process("new-student-email-template.html", thymeleafContext);

        System.out.println(htmlBody);

        return htmlBody;
    }

    public void sendNewFileUploadedEmail(Course course) {
        List<User> users = userRepository.findAll();
        Stream<User> usersEnrolled = users.stream().filter(user -> user.getCoursesAttended().contains(course));
        for (User user : usersEnrolled.collect(Collectors.toList())) {
            try {
                sendEmail(composeNewCourseEmailBody(user, course.getName()),user.getEmail(), SUBJECT_NEW_COURSE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendNewStudentEnrolled(Course course, User user) {
        try {
            sendEmail(composeNewStudentEmailBody(course, user), course.getTeacher().getEmail(), SUBJECT_NEW_STUDENT);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendEmail(String body, String to, String subject) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailConfig.getHost());
        mailSender.setPort(emailConfig.getPort());
        mailSender.setUsername(emailConfig.getUser());
        mailSender.setPassword(emailConfig.getPass());


        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(FROM);
        mailMessage.setTo(to);
        mailMessage.setText(body);
        mailMessage.setSubject(subject);

        mailSender.send(mailMessage);
    }
}