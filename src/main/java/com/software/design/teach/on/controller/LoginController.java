package com.software.design.teach.on.controller;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.model.dto.UserLoginDTO;
import com.software.design.teach.on.model.dto.UserRegisterDTO;
import com.software.design.teach.on.model.response.JwtAuthenticationResponse;
import com.software.design.teach.on.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/auth")
public class LoginController {

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);
    @Autowired
    private UserService userService;


    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginDTO userLoginDTO) throws BusinessException {
        String jwt = userService.login(userLoginDTO);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }


    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegisterDTO userRegisterDTO) throws BusinessException {
        try {
            userService.registerUser(userRegisterDTO);
            return ResponseEntity.ok(HttpStatus.CREATED);
        } catch (BusinessException be) {
            return ResponseEntity.badRequest().build();
        }
    }
}