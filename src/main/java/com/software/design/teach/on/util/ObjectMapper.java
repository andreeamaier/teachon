package com.software.design.teach.on.util;

import com.software.design.teach.on.model.Course;
import com.software.design.teach.on.model.Domain;
import com.software.design.teach.on.model.File;
import com.software.design.teach.on.model.User;
import com.software.design.teach.on.model.dto.*;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ObjectMapper {

    public static User userDtoToEntityMapper(UserRegisterDTO userDto) {
        User userEntity = new User();
        userEntity.setFirstName(userDto.getFirstName());
        userEntity.setLastName(userDto.getLastName());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setPassword(userDto.getPassword());

        return userEntity;
    }

    public static StudentDto userEntityToStudentDto(User user) {
        StudentDto student = new StudentDto();
        student.setFirstName(user.getFirstName());
        student.setLastName(user.getLastName());
        student.setEmail(user.getEmail());
        student.setDomainsOfInterest(domainEntityToDomainDto(user.getDomainsOfInterest()));

        Set<CourseDto> courses = new HashSet<>();
        user.getCoursesAttended().forEach(course -> courses.add(convertToCourseDto(course)));
        student.setCoursesEnrolled(courses);

        return student;
    }

    public static User studentDtoToUser(StudentDto studentDto){
        User user = new User();
        user.setEmail(studentDto.getEmail());
        user.setFirstName(studentDto.getFirstName());
        user.setLastName(studentDto.getLastName());

        return user;
    }

    private static Set<DomainDto> domainEntityToDomainDto(Set<Domain> domains) {
        Set<DomainDto> domainDtoSet = new HashSet<>();
        domains.forEach(domain -> domainDtoSet.add(convertToDomainDto(domain)));

        return domainDtoSet;
    }

    private static DomainDto convertToDomainDto(Domain domain) {
        DomainDto domainDto = new DomainDto();
        domainDto.setName(domain.getName());

        return domainDto;
    }
    private static Set<CourseDto> courseEntityToCourseDto(Set<Course> courses) {
        Set<CourseDto> courseDtoSet = new HashSet<>();
        courses.forEach(course -> courseDtoSet.add(convertToCourseDto(course)));

        return courseDtoSet;
    }

    public static CourseDto convertToCourseDto(Course course) {
        CourseDto courseDto = new CourseDto();
        courseDto.setName(course.getName());
        // todo set other fields

        return courseDto;
    }

    public static CourseAddAndSearchDto convertToCourseAddSearchDto(Course course) {
        CourseAddAndSearchDto courseDto = new CourseAddAndSearchDto();
        courseDto.setName(course.getName());

        return courseDto;
    }

    public static Course convertCourseAddDtoToCourseEntity(CourseAddAndSearchDto courseAddDto, Domain domain) {
        Course course = new Course();
        course.setName(courseAddDto.getName());
        course.setDomain(domain);

        return course;
    }

    public static FileUploadDto convertToFileDto(File file) {
        FileUploadDto fileDto = new FileUploadDto();
        fileDto.setTitle(file.getTitle());

        return fileDto;
    }

    public static File convertToFileEntity(FileUploadDto fileDto) {
        File file = new File();
        file.setTitle(fileDto.getTitle());

        return file;
    }
}
