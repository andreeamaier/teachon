package com.software.design.teach.on.model.dto;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadDto {

    private String title;
    private MultipartFile file;

    public FileUploadDto() {
    }

    public FileUploadDto(String title, MultipartFile file) {
        this.title = title;
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
