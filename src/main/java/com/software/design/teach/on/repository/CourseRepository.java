package com.software.design.teach.on.repository;

import com.software.design.teach.on.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    @Override
    Optional<Course> findById(Long aLong);
}
