package com.software.design.teach.on.controller;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.model.dto.StudentDto;
import com.software.design.teach.on.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('STUDENT')")
    public ResponseEntity<StudentDto> getUserProfile(@PathVariable(value = "id") Long id) throws BusinessException {
        try {
            return ResponseEntity.ok(userService.getStudentById(id));
        } catch (BusinessException be) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasRole('STUDENT') or hasRole('TEACHER')")
    public ResponseEntity<StudentDto> changeProfile(@RequestBody StudentDto studentDto, @PathVariable(value = "id") Long id) {
        try {
            return ResponseEntity.ok(userService.changeProfile(studentDto, id));
        } catch (BusinessException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
