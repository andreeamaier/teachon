package com.software.design.teach.on.controller;

import com.software.design.teach.on.model.dto.CourseAddAndSearchDto;
import com.software.design.teach.on.security.UserPrincipal;
import com.software.design.teach.on.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    CourseService courseService;

    //Domains will be already defined in db
    @PostMapping("/add/{domainId}")
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<?> addCourse(@RequestBody CourseAddAndSearchDto courseAddDto, @PathVariable Long domainId) {
        courseService.addCourse(courseAddDto, domainId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/enrollToCourse/{courseId}")
    @PreAuthorize("hasRole('STUDENT')")
    public ResponseEntity<?> enrollToCourse(@PathVariable Long courseId, @AuthenticationPrincipal UserPrincipal user) {
        courseService.enrollToCourse(courseId, user.getUser());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/unenrollFromCourse/{courseId}")
    @PreAuthorize("hasRole('STUDENT')")
    public ResponseEntity<?> unenrollFromCourse(@PathVariable Long courseId, @AuthenticationPrincipal UserPrincipal user) {
        courseService.unenrollFromCourse(courseId, user.getUser());
        return ResponseEntity.ok().build();
    }

}
