package com.software.design.teach.on.service;

import com.software.design.teach.on.config.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface FileService {

    void addNewFile(MultipartFile file, String title, Long courseId);
}
