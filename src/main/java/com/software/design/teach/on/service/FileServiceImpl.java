package com.software.design.teach.on.service;

import com.software.design.teach.on.config.BusinessException;
import com.software.design.teach.on.model.Course;
import com.software.design.teach.on.model.File;
import com.software.design.teach.on.model.dto.FileUploadDto;
import com.software.design.teach.on.repository.CourseRepository;
import com.software.design.teach.on.repository.FileRepository;
import com.software.design.teach.on.util.FileUtil;
import com.software.design.teach.on.util.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

@Component
public class FileServiceImpl implements FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private EmailService emailService;

    @Override
    public void addNewFile(MultipartFile file, String title , Long courseId) {
        String location = saveFileToDisk(file);
        File savedFile = new File();
        savedFile.setTitle(title);
        savedFile.setLocation(location);
        Course course = courseRepository.findById(courseId).get();
        savedFile.setCourse(course);
        Set<File> files = course.getFiles();
        files.add(savedFile);

        courseRepository.saveAndFlush(course);
        fileRepository.save(savedFile);

        emailService.sendNewFileUploadedEmail(course);
    }

    public String saveFileToDisk(MultipartFile file) {
        String path = null;
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            path = "C:\\Users\\andre\\Desktop\\master\\an1\\sem2\\SD\\teach.on\\teach.on\\static";

            try {

                FileUtil.fileUpload(file.getBytes(), path, fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path;
    }
}
