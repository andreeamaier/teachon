package com.software.design.teach.on.model.dto;

public class DomainDto {
    private String name;

    public DomainDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
