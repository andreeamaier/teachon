package com.software.design.teach.on.model.dto;

import java.util.Set;

public class StudentDto extends UserDto {

    private Set<DomainDto> domainsOfInterest;
    private Set<CourseDto> coursesEnrolled;

    public StudentDto() {
    }

    public Set<CourseDto> getCoursesEnrolled() {
        return coursesEnrolled;
    }

    public void setCoursesEnrolled(Set<CourseDto> coursesEnrolled) {
        this.coursesEnrolled = coursesEnrolled;
    }

    public Set<DomainDto> getDomainsOfInterest() {
        return domainsOfInterest;
    }

    public void setDomainsOfInterest(Set<DomainDto> domainsOfInterest) {
        this.domainsOfInterest = domainsOfInterest;
    }
}
