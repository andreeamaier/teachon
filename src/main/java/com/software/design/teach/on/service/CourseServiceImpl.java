package com.software.design.teach.on.service;

import com.software.design.teach.on.model.Course;
import com.software.design.teach.on.model.Domain;
import com.software.design.teach.on.model.User;
import com.software.design.teach.on.model.dto.CourseAddAndSearchDto;
import com.software.design.teach.on.repository.CourseRepository;
import com.software.design.teach.on.repository.DomainRepository;
import com.software.design.teach.on.repository.UserRepository;
import com.software.design.teach.on.util.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    DomainRepository domainRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailService emailService;

    @Override
    public void addCourse(CourseAddAndSearchDto courseDto, Long domainId) {
        Domain domain = domainRepository.findById(domainId).get();
        Course course = ObjectMapper.convertCourseAddDtoToCourseEntity(courseDto, domain);

        courseRepository.saveAndFlush(course);
    }

    @Override
    public void enrollToCourse(Long courseId, User user) {
        Course course = courseRepository.findById(courseId).get();
        Set<User> students = course.getStudents();
        students.add(user);
        courseRepository.saveAndFlush(course);

        Set<Course> courses = user.getCoursesAttended();
        if (!courses.contains(course)) {
            courses.add(course);
            userRepository.saveAndFlush(user);
        }

        emailService.sendNewStudentEnrolled(course, user);
    }

    @Override
    public List<CourseAddAndSearchDto> findCourse(String coursePartialName) {
        Set<Course> courses = new HashSet<>(courseRepository.findAll());
        List<Course> coursesMatchingName = courses.stream()
                .filter(course -> course.getName().toLowerCase().contains(coursePartialName.toLowerCase()))
                .collect(Collectors.toList());
        List<CourseAddAndSearchDto> courseDtos = new ArrayList<>();
        coursesMatchingName.forEach(course -> courseDtos.add(ObjectMapper.convertToCourseAddSearchDto(course)));
        return courseDtos;
    }

    @Override
    public void unenrollFromCourse(Long courseId, User user) {
        Course course = courseRepository.findById(courseId).get();
        Set<User> students = course.getStudents();

        for (User u : students ){
            if (u.getId().equals(user.getId())) {
                students.remove(u);
            }
        }
        courseRepository.saveAndFlush(course);

        Set<Course> courses = user.getCoursesAttended();
       for (Course c: courses) {
           if (c.getId().equals(courseId)) {
               courses.remove(c);
           }
       }
        userRepository.saveAndFlush(user);
    }
}
