package com.software.design.teach.on.controller;

import com.software.design.teach.on.model.dto.CourseAddAndSearchDto;
import com.software.design.teach.on.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private CourseService courseService;

    @GetMapping()
    @PreAuthorize("hasRole('STUDENT')")
    public ResponseEntity<?> searchCourseByName(@RequestParam String coursePartialName) {
        List<CourseAddAndSearchDto> courses = courseService.findCourse(coursePartialName);

        if (courses != null && !courses.isEmpty()) {
            return ResponseEntity.ok(courses);
        } else
            return ResponseEntity.notFound().build();
    }
}
