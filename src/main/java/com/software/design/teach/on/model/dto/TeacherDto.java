package com.software.design.teach.on.model.dto;

import java.util.Set;

public class TeacherDto extends UserDto {

    private Set<CourseDto> courses;

    public TeacherDto() {
    }

    public Set<CourseDto> getCourses() {
        return courses;
    }

    public void setCourses(Set<CourseDto> courses) {
        this.courses = courses;
    }
}
